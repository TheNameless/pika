package stream.example.com.pika;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

public class PikaView extends GLSurfaceView {

    private PikaRenderer renderer;

    public PikaView(Context context) {
        super(context);
    }

    public PikaView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setRenderer(Renderer renderer) {
        super.setRenderer(renderer);
        this.renderer = (PikaRenderer) renderer;
        this.renderer.setEyeX((angle)/100);//renderer.getEyeX() + -dX / 3000f);
    }

    private float startX;
    private float startY;
    private float angle=80f;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                Log.d("qqqqqq", "ACTION_DOWN: " + event);
                return true;

            case MotionEvent.ACTION_MOVE:
                float dX = event.getX() - startX;
                float dY = event.getY() - startY;

                renderer.setEyeY(renderer.getEyeY() + dY / 3000f);
               // if (angle != 0) {
                    renderer.setEyeX((angle + dX)/100);//renderer.getEyeX() + -dX / 3000f);

//                }
//                renderer.setEyeX(dX/100);//renderer.getEyeX() + -dX / 3000f);
                break;

                case MotionEvent.ACTION_UP:
                    angle = angle + (event.getX() - startX);
        }

        return super.onTouchEvent(event);

    }
}
