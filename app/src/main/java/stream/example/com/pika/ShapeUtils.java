package stream.example.com.pika;

import android.util.Log;

import java.util.Arrays;
import java.util.stream.Stream;

public class ShapeUtils {

    public static final int CIRCLE_VERTEX_COUNT = 30;

    public static float[] getCircle(float radius, float center_x, float center_y) {
// Create a buffer for vertex data
        float[] buffer = new float[CIRCLE_VERTEX_COUNT * 5]; // (x,y) for each vertex
        int idx = 0;

// Center vertex for triangle fan
        buffer[idx++] = center_x;
        buffer[idx++] = center_y;
        buffer[idx++] = 0.31f;
        buffer[idx++] = (1 + center_x) / 2;
        buffer[idx++] = (1 + center_y) / 2;
        Log.d("eeeee", "getCircle: center" + center_x + ", " + center_y);

// Outer vertices of the circle
        int outerVertexCount = CIRCLE_VERTEX_COUNT - 1;

        for (int i = 0; i < outerVertexCount; ++i) {
            float percent = (i / (float) (outerVertexCount - 1));
            float rad = (float) (percent * 2 * Math.PI);

            //Vertex position
            float outer_x = (float) (center_x + radius * Math.cos(rad));
            float outer_y = (float) (center_y + radius * Math.sin(rad));
            Log.d("eeeee", "getCircle: " + i + ", " + outer_x + ", " + outer_y + ", rad " + rad);

            buffer[idx++] = outer_x;
            buffer[idx++] = outer_y;
            buffer[idx++] = 0.31f;
            buffer[idx++] = (1 + outer_x) / 2;
            buffer[idx++] = (1 + outer_y) / 2;
        }

        return buffer;
    }

    public static float[] getArch(float radius, float center_x, float center_y) {
// Create a buffer for vertex data
        float[] buffer = new float[CIRCLE_VERTEX_COUNT * 5]; // (x,y) for each vertex
        int idx = 0;

// Center vertex for triangle fan
        buffer[idx++] = center_x;
        buffer[idx++] = center_y;
        buffer[idx++] = 0.31f;
        buffer[idx++] = (1 + center_x) / 2;
        buffer[idx++] = (1 + center_y) / 2;

// Outer vertices of the circle
        int outerVertexCount = CIRCLE_VERTEX_COUNT - 1;

//        for (int i = 0; i < outerVertexCount; ++i) {
//            float percent = (i / (float) (outerVertexCount - 1));
//            float rad = (float) (percent * 2 * Math.PI);
//
//            //Vertex position
//            float outer_x = (float) (center_x + radius * Math.cos(rad));
//            float outer_y = (float) (center_y + radius * Math.sin(rad));
//
//            if (outer_y > center_y) {
//
//                float bottomPoint = radius / 2;
//
//                if (outer_x > center_x - 0.005 && outer_x < center_x + 0.005) {
//                    outer_y = center_y;
//                }
////                else if (Math.abs(outer_x) > bottomPoint - 0.005 && Math.abs(outer_x) < bottomPoint - 0.005) {
////                    outer_y = center_y - radius / 2f;
////                }
//                else if (outer_x <= -bottomPoint) {
//                    float toBottom = Math.abs(outer_y - center_y) / 2f;
//                    outer_y = center_y - toBottom;
//                } else if (outer_x >= -bottomPoint && outer_x < center_x) {
//                    outer_y = outer_y - radius;
//                } else if (outer_x >= center_x && outer_x < bottomPoint) {
//                    outer_y = outer_y - radius;
//
//                } else if (outer_x >= bottomPoint) {
//                    float toBottom = Math.abs(outer_y - center_y) / 2f;
//                    outer_y = center_y - toBottom;
//
//                }
//
//            } else {
//                Log.d("qqqqq", "getArch: " + outer_x + ", " + outer_y);
//            }
//            buffer[idx++] = outer_x;
//            buffer[idx++] = outer_y;
//            buffer[idx++] = 0.31f;
//        }

        float[] mouth = {
                0.15f, -0.15f, 0.31f, 0.575f, 0.425f,
                0.146f, -0.17f, 0.31f, 0.573f, 0.415f,
                0.146f, -0.19f, 0.31f, 0.573f, 0.405f,
                0.11f, -0.18f, 0.31f, 0.555f, 0.41f,
                0.11f, -0.22f, 0.31f, 0.555f, 0.39f,
                0.1f, -0.19f, 0.31f, 0.55f, 0.405f,
                0.1f, -0.23f, 0.31f, 0.55f, 0.385f,
                0.08f, -0.2f, 0.31f, 0.54f, 0.4f,
                0.085f, -0.25f, 0.31f, 0.5425f, 0.275f,
                0.065f, -0.2f, 0.31f, 0.5325f, 0.4f,
                0.06f, -0.27f, 0.31f, 0.53f, 0.365f,
                0.037f, -0.19f, 0.31f, 0.5185f, 0.405f,
                0.02f, -0.28f, 0.31f, 0.51f, 0.36f,
                0f, -0.13f, 0.31f, 0.5f, 0.435f,
                -0.02f, -0.28f, 0.31f, 0.49f, 0.36f,
                -0.037f, -0.19f, 0.31f, 0.4815f, 0.405f,
                -0.06f, -0.27f, 0.31f, 0.47f, 0.365f,
                -0.065f, -0.2f, 0.31f, 0.4675f, 0.4f,
                -0.085f, -0.25f, 0.31f, 0.4575f, 0.275f,
                -0.08f, -0.2f, 0.31f, 0.46f, 0.4f,
                -0.1f, -0.23f, 0.31f, 0.45f, 0.385f,
                -0.1f, -0.19f, 0.31f, 0.45f, 0.405f,
                -0.11f, -0.22f, 0.31f, 0.445f, 0.39f,
                -0.11f, -0.18f, 0.31f, 0.445f, 0.41f,
                -0.146f, -0.19f, 0.31f, 0.427f, 0.405f,
                -0.146f, -0.17f, 0.31f, 0.427f, 0.415f,
                -0.15f, -0.15f, 0.31f, 0.425f, 0.425f,


        };


        return mouth;
    }

    public static float[] merge(float[]... arrays) {
        int finalLength = 0;
        for (float[] array : arrays) {
            finalLength += array.length;
        }

        float[] dest = null;
        int destPos = 0;

        for (float[] array : arrays) {
            if (dest == null) {
                dest = Arrays.copyOf(array, finalLength);
                destPos = array.length;
            } else {
                System.arraycopy(array, 0, dest, destPos, array.length);
                destPos += array.length;
            }
        }
        return dest;
    }
}
