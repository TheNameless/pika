package stream.example.com.pika;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES32.*;
import static stream.example.com.pika.ShapeUtils.CIRCLE_VERTEX_COUNT;

public class PikaRenderer implements GLSurfaceView.Renderer {

    private Context context;
    private int programId;
    private FloatBuffer vertexData;
    private int uColorLocation;
    private int aPositionLocation;
    private int uMatrixLocation;
    private float[] mProjectionMatrix = new float[16];
    private float[] mViewMatrix = new float[16];
    private float[] mMatrix = new float[16];
    private float eyeX;
    private float eyeY;
    private float eyeZ = 1.5f;
    private int uTextureUnitLocation;
    private int texture;
    private int aTextureLocation;
    private int textureEar;
    private int textureCheek;
    private int textureMouth;
    private int textureEyes;
    private int textureTail;

    PikaRenderer(Context context) {
        this.context = context;
    }

    public void setEyeY(float eyeY) {
        if (eyeY > 5) {
            eyeY = 5;
        } else if (eyeY < -5) {
            eyeY = -5;
        }
        this.eyeY = eyeY;
    }

    public void setEyeX(float angle) {
        eyeX = (float) (Math.cos(angle) * 2f);
        eyeZ = (float) (Math.sin(angle) * 2f);
    }

    public float getEyeY() {
        return eyeY;
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        glEnable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        int vertexShaderId = ShaderUtils.createShader(context, GL_VERTEX_SHADER, R.raw.vertex_shader);
        int fragmentShaderId = ShaderUtils.createShader(context, GL_FRAGMENT_SHADER, R.raw.fragment_shader);
        programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        glUseProgram(programId);
        aPositionLocation = glGetAttribLocation(programId, "a_Position");
        aTextureLocation = glGetAttribLocation(programId, "a_Texture");
        uTextureUnitLocation = glGetUniformLocation(programId, "u_TextureUnit");
        uMatrixLocation = glGetUniformLocation(programId, "u_Matrix");

        prepareData();
        bindData();
        createViewMatrix();
    }

    private void createProjectionMatrix(int width, int height) {
        float ratio = 1;
        float left = -1;
        float right = 1;
        float bottom = -1;
        float top = 1;
        float near = 1;
        float far = 6;
        if (width > height) {
            ratio = (float) width / height;
            left *= ratio;
            right *= ratio;
        } else {
            ratio = (float) height / width;
            bottom *= ratio;
            top *= ratio;
        }

        Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
    }

    private void createViewMatrix() {
        // точка положения камеры
        //  eyeY = 0;
        //  eyeZ = 1.5f;

        // точка направления камеры
        float centerX = 0;
        float centerY = 0;
        float centerZ = 0;

        // up-вектор
        float upX = 0;
        float upY = 1;
        float upZ = 0;

        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    }

    private void bindMatrix() {
        Matrix.multiplyMM(mMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);
        glUniformMatrix4fv(uMatrixLocation, 1, false, mMatrix, 0);
    }

    private void prepareData() {

        float[] rightEye = ShapeUtils.getCircle(0.08f, 0.3f, 0.12f);
        float[] leftEye = ShapeUtils.getCircle(0.08f, -0.3f, 0.12f);

        float[] rightCheek = ShapeUtils.getCircle(0.14f, 0.5f, -0.1f);
        float[] leftCheek = ShapeUtils.getCircle(0.14f, -0.5f, -0.1f);

        float[] mouth = ShapeUtils.getArch(0.15f, 0.0f, -0.15f);
        float[] vertices = {
                //back body
                0.0f, -0.4f, -0.3f, 0.5f, 0.3f,
                -0.4f, 0.4f, -0.3f, 0.3f, 0.7f,
                0.4f, 0.4f, -0.3f, 0.7f, 0.7f,
                0.8f, -0.3f, -0.3f, 0.9f, 0.4f,
                0.4f, -0.8f, -0.3f, 0.7f, 0.1f,
                -0.4f, -0.8f, -0.3f, 0.3f, 0.1f,
                -0.8f, -0.3f, -0.3f, 0.1f, 0.4f,
                -0.4f, 0.4f, -0.3f, 0.3f, 0.7f,
                //front body
                0.0f, -0.4f, 0.3f, 0.5f, 0.3f,
                -0.4f, 0.4f, 0.3f, 0.3f, 0.7f,
                0.4f, 0.4f, 0.3f, 0.7f, 0.7f,
                0.8f, -0.3f, 0.3f, 0.9f, 0.4f,
                0.4f, -0.8f, 0.3f, 0.7f, 0.1f,
                -0.4f, -0.8f, 0.3f, 0.3f, 0.1f,
                -0.8f, -0.3f, 0.3f, 0.1f, 0.4f,
                -0.4f, 0.4f, 0.3f, 0.3f, 0.7f,

                //ears
                -0.6f, 0.53f, -0.05f, 0.2f, 0.765f,
                -0.48f, 0.55f, -0.05f, 0.26f, 0.775f,
                -0.42f, 0.38f, -0.05f, 0.29f, 0.69f,
                -0.35f, 0.36f, -0.05f, 0.325f, 0.68f,

                0.6f, 0.53f, -0.05f, 0.8f, 0.765f,
                0.48f, 0.55f, -0.05f, 0.74f, 0.775f,
                0.42f, 0.38f, -0.05f, 0.71f, 0.69f,
                0.35f, 0.36f, -0.05f, 0.675f, 0.68f,

                //ears
                -0.6f, 0.53f, 0.05f, 0.2f, 0.765f,
                -0.48f, 0.55f, 0.05f, 0.26f, 0.775f,
                -0.42f, 0.38f, 0.05f, 0.29f, 0.69f,
                -0.35f, 0.36f, 0.05f, 0.325f, 0.68f,

                0.6f, 0.53f, 0.05f, 0.8f, 0.765f,
                0.48f, 0.55f, 0.05f, 0.74f, 0.775f,
                0.42f, 0.38f, 0.05f, 0.71f, 0.69f,
                0.35f, 0.36f, 0.05f, 0.675f, 0.68f,

                0.8f, -0.3f, -0.3f, 0.9f, 0.35f,
                0.8f, -0.3f, 0.3f, 0.9f, 0.65f,
                0.4f, -0.8f, -0.3f, 0.7f, 0.35f,
                0.4f, -0.8f, 0.3f, 0.7f, 0.65f,

                -0.8f, -0.3f, -0.3f, 0.1f, 0.35f,
                -0.8f, -0.3f, 0.3f, 0.1f, 0.65f,
                -0.4f, -0.8f, -0.3f, 0.3f, 0.35f,
                -0.4f, -0.8f, 0.3f, 0.3f, 0.65f,

                0.8f, -0.3f, -0.3f, 0.9f, 0.35f,
                0.8f, -0.3f, 0.3f, 0.9f, 0.65f,
                0.4f, 0.4f, -0.3f, 0.7f, 0.35f,
                0.4f, 0.4f, 0.3f, 0.7f, 0.65f,

                -0.8f, -0.3f, -0.3f, 0.1f, 0.35f,
                -0.8f, -0.3f, 0.3f, 0.1f, 0.65f,
                -0.4f, 0.4f, -0.3f, 0.3f, 0.35f,
                -0.4f, 0.4f, 0.3f, 0.3f, 0.65f,

                0.4f, -0.8f, -0.3f, 0.7f, 0.35f,
                -0.4f, -0.8f, -0.3f, 0.3f, 0.35f,
                0.4f, -0.8f, 0.3f, 0.7f, 0.65f,
                -0.4f, -0.8f, 0.3f, 0.3f, 0.65f,

                -0.4f, 0.4f, -0.3f, 0.3f, 0.35f,
                0.4f, 0.4f, -0.3f, 0.7f, 0.35f,
                -0.4f, 0.4f, 0.3f, 0.3f, 0.65f,
                0.4f, 0.4f, 0.3f, 0.7f, 0.65f,

                // ears connections
                -0.35f, 0.36f, -0.05f, 0.325f, 0.68f,
                -0.35f, 0.36f, 0.05f, 0.325f, 0.68f,
                -0.6f, 0.53f, 0.05f, 0.2f, 0.765f,
                -0.6f, 0.53f, -0.05f, 0.2f, 0.765f,
                -0.42f, 0.38f, -0.05f, 0.29f, 0.69f,
                -0.42f, 0.38f, 0.05f, 0.29f, 0.69f,
                -0.48f, 0.55f, 0.05f, 0.26f, 0.775f,
                -0.48f, 0.55f, -0.05f, 0.26f, 0.775f,

                0.35f, 0.36f, -0.05f, 0.675f, 0.68f,
                0.35f, 0.36f, 0.05f, 0.675f, 0.68f,
                0.6f, 0.53f, 0.05f, 0.8f, 0.765f,
                0.6f, 0.53f, -0.05f, 0.8f, 0.765f,
                0.42f, 0.38f, -0.05f, 0.71f, 0.69f,
                0.42f, 0.38f, 0.05f, 0.71f, 0.69f,
                0.48f, 0.55f, 0.05f, 0.74f, 0.775f,
                0.48f, 0.55f, -0.05f, 0.74f, 0.775f,


                //ears tops
                0.57f, 0.6f, -0.05f, 0.785f, 0.8f,
                0.6f, 0.53f, -0.05f, 0.8f, 0.765f,
                0.48f, 0.55f, -0.05f, 0.74f, 0.775f,

                -0.57f, 0.6f, -0.05f, 0.215f, 0.8f,
                -0.6f, 0.53f, -0.05f, 0.2f, 0.765f,
                -0.48f, 0.55f, -0.05f, 0.26f, 0.775f,


                //ears top connections
                -0.6f, 0.53f, -0.05f, 0.2f, 0.765f,
                -0.6f, 0.53f, 0.05f, 0.2f, 0.765f,
                -0.57f, 0.6f, 0.05f, 0.215f, 0.8f,
                -0.57f, 0.6f, -0.05f, 0.215f, 0.8f,
                -0.48f, 0.55f, -0.05f, 0.26f, 0.775f,
                -0.48f, 0.55f, 0.05f, 0.26f, 0.775f,
                -0.6f, 0.53f, 0.05f, 0.2f, 0.765f,
                -0.6f, 0.53f, -0.05f, 0.2f, 0.765f,


                0.6f, 0.53f, -0.05f, 0.2f, 0.765f,
                0.6f, 0.53f, 0.05f, 0.2f, 0.765f,
                0.57f, 0.6f, 0.05f, 0.215f, 0.8f,
                0.57f, 0.6f, -0.05f, 0.215f, 0.8f,
                0.48f, 0.55f, -0.05f, 0.26f, 0.775f,
                0.48f, 0.55f, 0.05f, 0.26f, 0.775f,
                0.6f, 0.53f, 0.05f, 0.2f, 0.765f,
                0.6f, 0.53f, -0.05f, 0.2f, 0.765f,
                // ось X
                -1f, 0, 0, 0f, 0.5f,
                1f, 0, 0, 1f, 0.5f,

                // ось Y
                0, -1f, 0, 0.5f, 0f,
                0, 1f, 0, 0.5f, 1f,

                // ось Z
                0, 0, -1f, 0f, 1f,
                0, 0, 1f, 0.f, 1f,


                //ears tops
                0.57f, 0.6f, 0.05f, 0.785f, 0.8f,
                0.6f, 0.53f, 0.05f, 0.8f, 0.765f,
                0.48f, 0.55f, 0.05f, 0.74f, 0.775f,

                -0.57f, 0.6f, 0.05f, 0.215f, 0.8f,
                -0.6f, 0.53f, 0.05f, 0.2f, 0.765f,
                -0.48f, 0.55f, 0.05f, 0.26f, 0.775f,

                0.0f, 0.05f, 0.31f, 0.5f, 0.525f,
                -0.07f, 0.1f, 0.31f, 0.465f, 0.55f,
                0.07f, 0.1f, 0.31f, 0.535f, 0.55f,

                -0.2f, -0.5f, -0.3f, 0.4f, 0.25f,
                0.2f, -0.5f, -0.3f, 0.6f, 0.25f,
                -0.3f, -0.15f, -0.45f, 0.35f, 0.425f,
                -0.15f, -0.15f, -0.45f, 0.425f, 0.425f,
                -0.15f, 0.1f, -0.52f, 0.425f, 0.55f,
                0.1f, 0.1f, -0.52f, 0.55f, 0.55f,
                -0.25f, 0.5f, -0.58f, 0.375f, 0.75f,

                -0.2f, -0.5f, -0.34f, 0.4f, 0.25f,
                0.2f, -0.5f, -0.34f, 0.6f, 0.25f,
                -0.3f, -0.15f, -0.48f, 0.35f, 0.425f,
                -0.15f, -0.15f, -0.48f, 0.425f, 0.425f,
                -0.15f, 0.1f, -0.55f, 0.425f, 0.55f,
                0.1f, 0.1f, -0.55f, 0.55f, 0.55f,
                -0.25f, 0.5f, -0.58f, 0.375f, 0.75f,

                -0.2f, -0.5f, -0.3f, 0.4f, 0.25f,
                -0.2f, -0.5f, -0.34f, 0.4f, 0.25f,
                0.2f, -0.5f, -0.3f, 0.6f, 0.25f,
                0.2f, -0.5f, -0.34f, 0.6f, 0.25f,
                -0.3f, -0.15f, -0.45f, 0.35f, 0.425f,
                -0.3f, -0.15f, -0.48f, 0.35f, 0.425f,
                -0.15f, -0.15f, -0.45f, 0.425f, 0.425f,
                -0.15f, -0.15f, -0.48f, 0.425f, 0.425f,
                -0.15f, 0.1f, -0.52f, 0.425f, 0.55f,
                -0.15f, 0.1f, -0.55f, 0.425f, 0.55f,
                0.1f, 0.1f, -0.52f, 0.55f, 0.55f,
                0.1f, 0.1f, -0.55f, 0.55f, 0.55f,
                -0.25f, 0.5f, -0.58f, 0.75f, 0.55f,

        };

        float[] both = ShapeUtils.merge(vertices, leftEye, rightEye, leftCheek, rightCheek, mouth);
        vertexData = ByteBuffer.allocateDirect(both.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        vertexData.put(both);

        texture = TextureUtils.loadTexture(context, R.drawable.texture, GL_TEXTURE0);
        textureEar = TextureUtils.loadTexture(context, R.drawable.brown, GL_TEXTURE1);
        textureMouth = TextureUtils.loadTexture(context, R.drawable.mouth, GL_TEXTURE2);
        textureCheek = TextureUtils.loadTexture(context, R.drawable.cheeks, GL_TEXTURE3);
        textureEyes = TextureUtils.loadTexture(context, R.drawable.eyes, GL_TEXTURE4);
        textureTail = TextureUtils.loadTexture(context, R.drawable.tail, GL_TEXTURE5);
    }

    private void bindData() {
        vertexData.position(0);
        glVertexAttribPointer(aPositionLocation, 3, GL_FLOAT, false, 20, vertexData);
        glEnableVertexAttribArray(aPositionLocation);

        vertexData.position(3);
        glVertexAttribPointer(aTextureLocation, 2, GL_FLOAT,
                false, 20, vertexData);
        glEnableVertexAttribArray(aTextureLocation);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glBindTexture(GL_TEXTURE_2D, textureEar);
        glBindTexture(GL_TEXTURE_2D, textureEyes);
        glBindTexture(GL_TEXTURE_2D, textureMouth);
        glBindTexture(GL_TEXTURE_2D, textureCheek);
        glUniform1i(uTextureUnitLocation, 0);
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        glViewport(0, 0, width, height);
        createProjectionMatrix(width, height);
        bindMatrix();
    }

    private int vertexCount;

    private void drawFan(int vertex) {
        glDrawArrays(GL_TRIANGLE_FAN, vertexCount, vertex);
        vertexCount = vertexCount + vertex;
    }

    private void drawTriangles(int vertex) {
        glDrawArrays(GL_TRIANGLES, vertexCount, vertex);
        vertexCount = vertexCount + vertex;
    }

    private void drawStrip(int vertex) {
        glDrawArrays(GL_TRIANGLE_STRIP, vertexCount, vertex);
        vertexCount = vertexCount + vertex;
    }

    private void drawLine() {
        glDrawArrays(GL_LINES, vertexCount, 2);
        vertexCount = vertexCount + 2;
    }

    @Override
    public void onDrawFrame(GL10 gl10) {

        createViewMatrix();
        bindMatrix();
        glClearColor(1f, 1f, 1f, 1f);

        vertexCount = 0;

        setFurTexture();
        drawFan(8);
        drawFan(8);

        drawStrip(4);
        drawStrip(4);
        drawStrip(4);
        drawStrip(4);
        drawStrip(4);
        drawStrip(4);
        drawStrip(4);
        drawStrip(4);
        drawStrip(4);
        drawStrip(4);

        drawStrip(8);
        drawStrip(8);

        setEarsTexture();
        drawTriangles(6);
        drawStrip(8);
        drawStrip(8);

        // оси
        drawAxises();

        drawTriangles(6);

        //nose
        drawTriangles(3);

        setTailTexture();
        drawStrip(7);
        drawStrip(7);
        drawStrip(13);

        setEyesTexture();
        drawFan(CIRCLE_VERTEX_COUNT);
        drawFan(CIRCLE_VERTEX_COUNT);

        setCheeksTexture();
        drawFan(CIRCLE_VERTEX_COUNT);
        drawFan(CIRCLE_VERTEX_COUNT);

        setMouthTexture();
        drawStrip(27);
    }

    private void drawAxises() {
        glLineWidth(1);

        glUniform4f(uColorLocation, 0.0f, 1.0f, 1.0f, 1.0f);
        drawLine();

        glUniform4f(uColorLocation, 1.0f, 0.0f, 1.0f, 1.0f);
        drawLine();

        glUniform4f(uColorLocation, 1.0f, 0.5f, 0.0f, 1.0f);
        drawLine();
    }

    private void setFurTexture() {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glUniform1i(uTextureUnitLocation, 0);
    }

    private void setTailTexture() {
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, textureTail);
        glUniform1i(uTextureUnitLocation, 5);
    }

    private void setEarsTexture() {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureEar);
        glUniform1i(uTextureUnitLocation, 1);
    }

    private void setEyesTexture() {
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, textureEyes);
        glUniform1i(uTextureUnitLocation, 4);
    }

    private void setCheeksTexture() {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureCheek);
        glUniform1i(uTextureUnitLocation, 3);
    }

    private void setMouthTexture() {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureMouth);
        glUniform1i(uTextureUnitLocation, 2);
    }
}
